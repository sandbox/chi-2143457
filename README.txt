-= Devel debug bar module =-

Summary
=========================
This module provides integration with PHP DebugBar library.
The module integrates easily in any projects and can display
profiling data from any part of your application. It comes built-in with
data collectors for standard PHP features and popular projects.

Requirements
=========================
 * PHP 5.3 or higher
 * PHP DebugBar library should be installed to sites/all/libraries/vendor directory.

Installation
=========================
 These instructions assumes that the you have installed composer globally on your system.

 * Copy composer.json to libraries directory:
   cp sites/all/modules/devel/devel_debug_bar/composer.json sites/all/libraries
 * Install PHP Debug library:
   (cd sites/all/libraries && composer install)
 * Enable the module:
   drush en devel_debug_bar

Links
=========================
 * PHP DebugBar home page: http://phpdebugbar.com
 * Drush home page: http://www.drush.org
 * Composer home page: http://getcomposer.org
