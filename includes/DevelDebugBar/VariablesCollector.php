<?php

/**
 *
 */

namespace DevelDebugBar;

use DebugBar\DataCollector\RequestDataCollector;
use DebugBar\DataCollector\Renderable;

/**
 * Collects info about Drupal variables.
 */
class VariablesCollector extends RequestDataCollector implements Renderable {

  /**
   * {@inheritDoc}
   */
  public function collect()   {
    global $conf;
    return $conf;
  }

  /**
   * {@inheritDoc}
   */
  public function getName() {
    return 'variables';
  }

  /**
   * {@inheritDoc}
   */
  public function getWidgets() {
    return array(
      "variables" => array(
        "widget" => "PhpDebugBar.Widgets.VariableListWidget",
        "map" => "variables",
        "default" => "{}"
      ),
    );
  }

}
