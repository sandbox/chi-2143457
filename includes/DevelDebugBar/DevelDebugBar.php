<?php

namespace DevelDebugBar;

use DebugBar\DebugBar;
use DebugBar\DataCollector\PhpInfoCollector;
use DebugBar\DataCollector\MessagesCollector;
use DebugBar\DataCollector\TimeDataCollector;
use DebugBar\DataCollector\RequestDataCollector;
use DebugBar\DataCollector\MemoryCollector;
use DebugBar\DataCollector\ExceptionsCollector;

use DevelDebugBar\EntitiesCollector;
use DevelDebugBar\WatchdogCollector;
use DevelDebugBar\VariablesCollector;
use DevelDebugBar\IframeCollector;
use DevelDebugBar\GitCollector;


/**
 *
 */
class DevelDebugBar extends DebugBar{

  protected static $instance;

  /**
   *
   */
  public function __construct() {

    if (self::$instance) {
      throw new \Exception('Debug bar instance already exists');
    }

    // PHP debug bar collectors.
    $this->addCollector(new PhpInfoCollector());
    $this->addCollector(new MessagesCollector());
    $this->addCollector(new RequestDataCollector());
    $this->addCollector(new TimeDataCollector());
    $this->addCollector(new MemoryCollector());

    // Devel collectors.
    $this->addCollector(new EntitiesCollector());
    $this->addCollector(new WatchdogCollector());
    $this->addCollector(new VariablesCollector());
    $this->addCollector(new StatusReportCollector());
    //$this->addCollector(new PerformanceCollector());

    $gitCollector = new GitCollector();
    if ($gitCollector->getBranch()) {
      $this->addCollector($gitCollector);
    }


    //$this->addCollector(new IframeCollector('admin/reports/status'));

    self::$instance = $this;
  }

  /**
   *
   */
  public static function getInstance() {
    if (!self::$instance) {
      self::$instance = new static();
    }
    return self::$instance;
  }

} 