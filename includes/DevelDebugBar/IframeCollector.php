<?php

/**
 *
 */

namespace DevelDebugBar;

use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;

/**
 * Collects info about Drupal variables.
 */
class IframeCollector extends DataCollector implements Renderable {

  private $url;

  public function __construct($url) {
    $this->url = $url;
  }

  /**
   * {@inheritDoc}
   */
  public function collect()   {
    return url($this->url);
  }

  /**
   * {@inheritDoc}
   */
  public function getName() {
    return 'iframe';
  }

  /**
   * {@inheritDoc}
   */
  public function getWidgets() {
    return array(
      "iframe" => array(
        "widget" => "PhpDebugBar.Widgets.IFrameWidget",
        "map" => "iframe",
        "default" => "{}"
      ),
    );
  }

}
