<?php

/**
 *
 */

namespace DevelDebugBar;

use DebugBar\DataCollector\RequestDataCollector;
use DebugBar\DataCollector\Renderable;

/**
 * Collects info about loaded entities.
 */
class EntitiesCollector extends RequestDataCollector implements Renderable {

  /**
   * {@inheritDoc}
   */
  public function collect()   {

    $loaded_entities = &drupal_static('devel_debug_bar_loaded_entities');

    $data = array();
    if ($loaded_entities) {
      foreach ($loaded_entities  as $entity_type => $entities) {
        foreach ($entities  as $entity) {
          list($id, ,$bundle) = entity_extract_ids($entity_type, $entity);
          $data["$entity_type ($bundle) #$id"] =  print_r($entity, TRUE);
        }
      }
    }
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getName() {
    return 'entities';
  }

  /**
   * {@inheritDoc}
   */
  public function getWidgets() {
    return array(
      "entities" => array(
        "widget" => "PhpDebugBar.Widgets.VariableListWidget",
        "map" => "entities",
        "default" => "{}"
      )
    );
  }

}
