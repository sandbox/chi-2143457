<?php

/**
 *
 */

namespace DevelDebugBar;

use DebugBar\DataCollector\RequestDataCollector;
use DebugBar\DataCollector\Renderable;

/**
 * Collects info about watchdog messages.
 */
class StatusReportCollector extends RequestDataCollector implements Renderable {

  /**
   * {@inheritDoc}
   */
  public function collect()   {
    module_load_include('inc', 'system', 'system.admin');
    return \system_status();
  }

  /**
   * {@inheritDoc}
   */
  public function getName() {
    return 'status';
  }

  /**
   * {@inheritDoc}
   */
  public function getWidgets() {
    return array(
      "Status report" => array(
        "widget" => "DevelDebugBar.Widgets.MarkupWidget",
        "map" => "status",
        "default" => "{}"
      )
    );
  }

}
